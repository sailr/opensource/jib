build:
	go build -o bin

cli: clean build
	bin/jib cli

infer: clean build
	bin/jib infer

clean:
	rm -rf jib-templates
	rm -rf manifests
