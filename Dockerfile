######
# NOTE: This is a [working] test file for jib grab the exposed port.
######

FROM golang:1.12.5 as builder

WORKDIR /workspace
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

# Copy the go source
COPY main.go main.go
COPY cmd/ cmd/
COPY pkg/ pkg/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o jib main.go

# Use distroless as minimal base image to package the jib binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/jib .
USER nonroot:nonroot

EXPOSE 3000

CMD ["/jib"]
