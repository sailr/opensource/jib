# jib

Kubernetes Manifest Builder [CLI tool]

## Problem Statement

Writing Kubernetes manifests isn't hard but it is tedious. Often users are left to copying examples from the web or using previous implementations and tweaking them to fit the use case.

jib stands to fit solve this problem by providing scaffolding for Kubernetes manifests. It does this in two distinct ways: making assumptions based on project layout or answering a series of questions via the CLI . 

## Documentation

Documentation can be found in the [SPEC.md](./docs/SPEC.md)

## Getting Started

`go get gitlab.com/sailr/opensource/jib`

`$GOPATH/bin/jib --help`
