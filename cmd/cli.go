package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/sailr/opensource/jib/pkg/generator"
	"gitlab.com/sailr/opensource/jib/pkg/prompt"
)

// Name of the application
var name string

// Registry is the container registry URI
var registry string

// ServiceType is the type of k8s service to use (default: LoadBalancer)
var serviceType string

// ImagePullSecret is the secret used for private container registry access
var imagePullSecret string

// AppRunPort is the port the application runs on
var appRunPort int

// ContainerExternallyExposedPort is the port exposed in the k8s pod
var containerExternallyExposedPort int

// AppReplicas is the number of k8s replicas to instantiate (default 3)
var appReplicas int

// RevisionHistoryLimit is the number of versions to keep in history (default 1)
var revisionHistoryLimit int

// cliCmd represents the cli command
var cliCmd = &cobra.Command{
	Use:   "cli",
	Short: "Cli prompt for setting up Kubernetes manifests",
	Long: `An interactive prompt will display asking Application-specific
questions for building out Kubernetes manifests.
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(name) > 0 {
			unattendedAppData := generator.ApplicationInfo{
				Name:                 name,
				Registry:             registry,
				AppRunPort:           appRunPort,
				Secret:               imagePullSecret,
				Replicas:             appReplicas,
				RevisionHistoryLimit: revisionHistoryLimit,
				AppExposedPort:       containerExternallyExposedPort,
				ServiceType:          serviceType,
			}
			generator.Generate(&unattendedAppData)
		} else {
			cliPromptData := prompt.Cli()
			generator.Generate(cliPromptData)
		}
	},
}

func init() {
	rootCmd.AddCommand(cliCmd)
	cliCmd.Flags().StringVarP(&name, "name", "n", "", "Name of the application")
	cliCmd.Flags().StringVarP(&registry, "registry", "r", "", "Registry is the container registry URI")
	cliCmd.Flags().StringVarP(&serviceType, "servicetype", "s", "LoadBalancer", "ServiceType is the type of k8s service to use")
	cliCmd.Flags().StringVarP(&imagePullSecret, "secret", "i", "", "ImagePullSecret is the secret used for private container registry access")
	cliCmd.Flags().IntVarP(&appRunPort, "runport", "a", 0, "AppRunPort is the port the application runs on")
	cliCmd.Flags().IntVarP(&containerExternallyExposedPort, "exposeport", "c", 0, "ContainerExternallyExposedPort is the port exposed in the k8s pod")
	cliCmd.Flags().IntVarP(&appReplicas, "replicas", "p", 3, "AppReplicas is the number of k8s replicas to instantiate")
	cliCmd.Flags().IntVarP(&revisionHistoryLimit, "revisionhistory", "l", 1, "RevisionHistoryLimit is the number of versions to keep in history")
}
