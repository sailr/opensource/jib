package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/sailr/opensource/jib/pkg/generator"
	"gitlab.com/sailr/opensource/jib/pkg/infer"
)

// inferCmd represents the infer command
var inferCmd = &cobra.Command{
	Use:   "infer",
	Short: "Infer uses environment clues to generate manifests",
	Long: `Infer uses inference techniques, optional flags, and defaults
to dervive Kubernetes manifests values.`,
	Run: func(cmd *cobra.Command, args []string) {
		app := generator.ApplicationInfo{
			Name:                 infer.AppName(),
			Registry:             "registry." + infer.Registry(),
			AppRunPort:           infer.RunPort(),
			Secret:               "",
			Replicas:             3,
			RevisionHistoryLimit: 1,
			AppExposedPort:       80,
			ServiceType:          "LoadBalancer",
		}

		generator.Generate(&app)
	},
}

func init() {
	rootCmd.AddCommand(inferCmd)
	inferCmd.Flags().StringVarP(&name, "name", "n", "", "Name of the application")
	inferCmd.Flags().StringVarP(&registry, "registry", "r", "", "Registry is the container registry URI")
	inferCmd.Flags().StringVarP(&serviceType, "servicetype", "s", "LoadBalancer", "ServiceType is the type of k8s service to use")
	inferCmd.Flags().StringVarP(&imagePullSecret, "secret", "i", "", "ImagePullSecret is the secret used for private container registry access")
	inferCmd.Flags().IntVarP(&appRunPort, "runport", "a", 0, "AppRunPort is the port the application runs on")
	inferCmd.Flags().IntVarP(&containerExternallyExposedPort, "exposeport", "c", 0, "ContainerExternallyExposedPort is the port exposed in the k8s pod")
	inferCmd.Flags().IntVarP(&appReplicas, "replicas", "p", 3, "AppReplicas is the number of k8s replicas to instantiate")
	inferCmd.Flags().IntVarP(&revisionHistoryLimit, "revisionhistory", "l", 1, "RevisionHistoryLimit is the number of versions to keep in history")
}
