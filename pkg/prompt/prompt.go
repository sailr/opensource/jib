package prompt

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"

	"github.com/manifoldco/promptui"

	"gitlab.com/sailr/opensource/jib/pkg/generator"
)

// Cli runs the series of prompts required to populate the manifests
func Cli() *generator.ApplicationInfo {
	/////////////////////////////////////
	// create the validation function //
	///////////////////////////////////
	validate := func(input string) error {
		rMatch := regexp.MustCompile("[a-z0-9A-Z]")
		if !rMatch.MatchString(input) { // TODO: https://www.alexedwards.net/blog/validation-snippets-for-go
			return errors.New("Input text does not match: [a-z0-9A-Z]")
		} else if len(input) < 1 {
			return errors.New("Entry must container characters")
		}
		return nil
	}

	//////////////////////////////////
	// prompt for Application name //
	////////////////////////////////
	appNamePrompt := promptui.Prompt{
		Label:    "Application Name ",
		Validate: validate,
	}
	appName, err2 := appNamePrompt.Run()
	if err2 != nil {
		fmt.Printf("Prompt failed %v\n", err2)
	}
	fmt.Printf("Your entry is %q\n", appName)

	////////////////////////////////////////
	// prompt for container registry URI //
	//////////////////////////////////////
	registryURIPrompt := promptui.Prompt{
		Label:    "Container Registry URI ",
		Validate: validate,
	}
	registryURI, err := registryURIPrompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
	}
	fmt.Printf("Your entry is %q\n", registryURI)

	///////////////////////////////////////
	// prompt for Application run port //
	////////////////////////////////////
	appRunPortPrompt := promptui.Prompt{
		Label:    "Application Running Port ",
		Validate: validate,
	}
	appRunPortString, err1 := appRunPortPrompt.Run()
	appRunPort, err1 := strconv.Atoi(appRunPortString)
	if err1 != nil {
		fmt.Printf("Prompt failed %v\n", err1)
	}
	fmt.Printf("Your entry is %q\n", appRunPort)

	/////////////////////////////////////////
	// prompt for externally exposed port //
	///////////////////////////////////////
	containnerExternallyExposedPrompt := promptui.Prompt{
		Label:    "Application Container Externally Exposed Port ",
		Validate: validate,
	}
	containerExternallyExposedPortString, err2 := containnerExternallyExposedPrompt.Run()
	containerExternallyExposedPort, err2 := strconv.Atoi(containerExternallyExposedPortString)
	if err2 != nil {
		fmt.Printf("Prompt failed %v\n", err2)
	}
	fmt.Printf("Your entry is %q\n", containerExternallyExposedPort)

	/////////////////////////////////////////////
	// prompt for number of replicas run port //
	///////////////////////////////////////////
	replicasPrompt := promptui.Prompt{
		Label:    "Number of App Replicas (Default: 3) ",
		Validate: validate,
	}
	appReplicasString, err3 := replicasPrompt.Run()
	appReplicas, err3 := strconv.Atoi(appReplicasString)
	if err3 != nil {
		fmt.Printf("Prompt failed %v\n", err3)
	}
	// 0 is unset or nil
	if appReplicas == 0 {
		appReplicas = 3
	}
	fmt.Printf("Your entry is %q\n", appReplicas)

	////////////////////////////////////////////
	// prompt for length of revision history //
	//////////////////////////////////////////
	revisionHistoryLimitPrompt := promptui.Prompt{
		Label: "Length of App Container History Secret (Default: 1) ",
	}
	revisionHistoryLimitString, err4 := revisionHistoryLimitPrompt.Run()
	revisionHistoryLimit, err4 := strconv.Atoi(revisionHistoryLimitString)
	if err4 != nil {
		fmt.Printf("Prompt failed %v\n", err4)
	}
	// 0 is unset or nil
	if revisionHistoryLimit == 0{
		revisionHistoryLimit = 1
	}
	fmt.Printf("your entry is %q\n", revisionHistoryLimit)

	///////////////////////////////////////
	// prompt for external service type //
	//////////////////////////////////////
	serviceTypePrompt := promptui.Prompt{
		Label: "Kubernetes Service Type (Default: LoadBalancer) ",
	}
	serviceType, err4 := serviceTypePrompt.Run()
	if err4 != nil {
		fmt.Printf("Prompt failed %v\n", err4)
	}
	if len(serviceType) == 0 {
		serviceType = "LoadBalancer"
	}
	fmt.Printf("your entry is %q\n", serviceType)

	//////////////////////////////////
	// prompt for Image Pull Secret //
	////////////////////////////////
	imagePullSecretPrompt := promptui.Prompt{
		Label: "Registry Secret (if none, leave blank) ",
	}
	imagePullSecret, err4 := imagePullSecretPrompt.Run()
	if err4 != nil {
		fmt.Printf("Prompt failed %v\n", err4)
	}
	fmt.Printf("Your entry is %q\n", imagePullSecret)

	return &generator.ApplicationInfo{
		Name:                 appName,
		Registry:             registryURI,
		AppRunPort:           appRunPort,
		AppExposedPort:       containerExternallyExposedPort,
		Replicas:             appReplicas,
		RevisionHistoryLimit: revisionHistoryLimit,
		ServiceType:          serviceType,
		Secret:               imagePullSecret,
	}
}
