package metatypes

// MetadataObject is the metadata object used
type MetadataObject struct {
	Name string `yaml:"name"`
}

// MetaManifestsObject is an object that contains all the required manifests
type MetaManifestsObject struct {
	Deployment *DeploymentObject
	Service *ServiceObject
	Kustomize *KustomizeObject
}
