package metatypes

import ()

// KustomizeObject is the entire kustomize object
type KustomizeObject struct {
	APIVersion   string `yaml:"apiVersion"`
	Kind         string `yaml:"kind"`
	NameSuffix   string `yaml:"nameSuffix"`
	CommonLabels struct {
		Name string `yaml:"name"`
	} `yaml:"commonLabels"`
	Resources []string `yaml:"resources"`
	Images    []struct {
		Name   string `yaml:"name"`
		NewTag string `yaml:"newTag"`
	} `yaml:"images"`
}
