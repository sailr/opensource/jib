package metatypes

import()

// ServiceObject is the entire service.yml as a struct
type ServiceObject struct {
	APIVersion string `yaml:"apiVersion"`
	Kind string `yaml:"kind"`
	Metadata struct {
		Name string `yaml:"name"`
	} `yaml:"metadata"`
	Spec struct {
		Ports []struct {
			Port int `yaml:"port"`
			TargetPort int `yaml:"targetPort"`
		} `yaml:"ports"`
		Type string `yaml:"type"`
	} `yaml:"spec"`
}
