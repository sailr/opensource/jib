package infer

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// AppName infers the application's name from the directory name
func AppName() string {
	// take everything after the last slash
	appNameRegex, _ := regexp.Compile("([^/]+$)")
	app, err := os.Getwd()
	if err != nil {
		log.Println("unable to get current working directory", err)
	}
	return appNameRegex.FindString(app)
}

// Registry infers the app's container registry URI
// from the .git config's origin(if !present; default dockerhub)
func Registry() string {
	gitConfig, err := os.Open(".git/config")
	if err != nil {
		log.Println("git config not found. Is it git initalized?", err)
	}
	reader := bufio.NewReader(gitConfig)
	var line string
	var registry string
	for {
		line, err = reader.ReadString('\n')
		if strings.Contains(line, "url") {
			registryRemoveAt := strings.Split(line, "@")
			registryRemoveGit := strings.Split(registryRemoveAt[1], ".git")
			registry = strings.Replace(registryRemoveGit[0], ":", "/", -1)
		}
		if err != nil {
			break
		}
	}
	return registry
}

// RunPort infers the app's EXPOSE command in the dockerfile (if !present; default 8080)
func RunPort() int {
	dockerfile, err := os.Open("./Dockerfile")
	defer dockerfile.Close()
	if err != nil {
		log.Println("error opening Dockerfile", err)
	}
	reader := bufio.NewReader(dockerfile)
	var line string
	var port int
	for {
		// remove all newlines, whitespace, and alpha-characters
		portRegex, _ := regexp.Compile("[a-zA-Z\\n\\s]")
		line, err = reader.ReadString('\n')
		if strings.Contains(line, "EXPOSE ") {
			portString := portRegex.ReplaceAllString(line, "")
			port, err = strconv.Atoi(portString)
			if err != nil {
				log.Println("error compiling regex", err)
			}
		}
		if err != nil {
			break
		}
	}
	return port
}
