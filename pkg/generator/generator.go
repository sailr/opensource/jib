package generator

import (
	"io/ioutil"
	"log"
	"os/exec"

	"gopkg.in/yaml.v2"

	"gitlab.com/sailr/opensource/jib/pkg/metatypes"
)

// ApplicationInfo is the information about an application
type ApplicationInfo struct {
	Name                 string
	Registry             string
	AppRunPort           int
	Secret               string
	Replicas             int
	RevisionHistoryLimit int
	AppExposedPort       int
	ServiceType          string
}

// Generate generates the manifests based on templates
func Generate(newApp *ApplicationInfo) {
	fetchTemplates()

	serviceObject := metatypes.ServiceObject{}
	service, err := ioutil.ReadFile("manifests/service.yml")
	err = yaml.Unmarshal([]byte(service), &serviceObject)

	serviceObject.Metadata.Name = newApp.Name
	serviceObject.Spec.Ports[0].Port = newApp.AppRunPort
	serviceObject.Spec.Ports[0].TargetPort = newApp.AppExposedPort
	serviceObject.Spec.Type = newApp.ServiceType

	deploymentObject := metatypes.DeploymentObject{}
	deployment, err := ioutil.ReadFile("manifests/deployment.yml")
	err = yaml.Unmarshal([]byte(deployment), &deploymentObject)

	deploymentObject.Metadata.Name = newApp.Name
	deploymentObject.Spec.Replicas = newApp.Replicas
	deploymentObject.Spec.RevisionHistoryLimit = newApp.RevisionHistoryLimit
	deploymentObject.Spec.Template.Metadata.Name = newApp.Name
	deploymentObject.Spec.Template.Spec.Containers[0].Name = newApp.Name
	deploymentObject.Spec.Template.Spec.Containers[0].Image = newApp.Registry
	deploymentObject.Spec.Template.Spec.Containers[0].Ports[0].ContainerPort = newApp.AppRunPort
	deploymentObject.Spec.Template.Spec.ImagePullSecrets[0].Name = newApp.Secret

	kustomizeObject := metatypes.KustomizeObject{}
	kustomize, err := ioutil.ReadFile("manifests/kustomization.yml")
	err = yaml.Unmarshal([]byte(kustomize), &kustomizeObject)

	kustomizeObject.NameSuffix = ""
	kustomizeObject.CommonLabels.Name = newApp.Name
	kustomizeObject.Images[0].Name = newApp.Registry
	kustomizeObject.Images[0].NewTag = ""

	log.Println("Generating Application Kubernetes Manifests...")

	var serviceMarshal, deploymentMarshal, kustomizeMarshal []byte

	serviceMarshal, err = yaml.Marshal(&serviceObject)
	err = ioutil.WriteFile("manifests/service.yml", serviceMarshal, 0644)
	deploymentMarshal, err = yaml.Marshal(&deploymentObject)
	err = ioutil.WriteFile("manifests/deployment.yml", deploymentMarshal, 0644)
	kustomizeMarshal, err = yaml.Marshal(&kustomizeObject)
	err = ioutil.WriteFile("manifests/kustomization.yml", kustomizeMarshal, 0644)

	if err != nil {
		log.Println(err)
	}
}

// fetchTemplates fetches the manifest templates from the open source project
func fetchTemplates() {
	jibTemplateURI := "git@gitlab.com:sailr/opensource/jib-templates.git"
	clone := exec.Command("git", "clone", jibTemplateURI)
	err := clone.Run()
	if err != nil {
		log.Println("clone error", err)
	}
	moveManifests := exec.Command("/bin/sh", "-c", "mv jib-templates/manifests .")
	err4 := moveManifests.Run()
	if err4 != nil {
		log.Println("error moving manifests", err)
	}
	removeGit := exec.Command("rm", "-rf", "jib-templates")
	err2 := removeGit.Run()
	if err2 != nil {
		log.Println("error removing files", err2)
	}
}
