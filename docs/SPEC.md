# jib

CLI tool for generating Kubernetes manifests, intelligently

## Overview

* `cli` - interactive command line interface
* `infer` - jib infers the configuration based on project layout

jib builds a base configuration of:

* deployment.yml
* service.yml
* kustomization.yml

While support for addition types will be added in the near future. These core types allow for an Application to be deployed and accessed in a cluster.

`metatypes` contains the types that make up the larger kubernetes objects.

### CLI

The user is prompt to answer questions regarding their application. This helps to fill out information in the respective manifests.

1. What is the name of your Application?
2. What is the registry URL to access your container image?
3. Is there a cluster secret associated with your container registry?
4. What port is your application running on?
5. Would you like to expose your Application externally?
6. How many replicas would you like in the cluster? Default: 1.
7. How many historical versions of the application would you like to keep for rollback? Default: 1.

The answers to these questions guide how the manifests are built. Parameterizing the available options.

Flags are available for an unattended option.

* `--name` - The application name
* `--registry` - The container registry URI
* `--secret` - The imagePullSecret (if private registry is used)
* `--runport` - The port the application runs on
* `--exposeport` - The port you want exposed from the luster
* `--servicetype` - The kubernetes service type
* `--replicas` - The number of replicas
* `--revisionhistory` - The number of container versions to keep

Example Command:

```go
bin/jib cli
  --name a
  --registry a
  --servicetype a
  --secret a
  --runport 80
  --exposeport 80
  --replicas 3
  --revisionhistory 1
```

## Infer

Infer makes inference about the parameters required to build Kubernetes manifests. It does this by using some preset defaults, and using the project structure to define unique values. Infer also uses flags to allow the user to pass-in values dynamically at runtime.

* Name - App directory name
* Registry - Attempt to use .git/config `[remote "origin"]` url value to assume
* ServiceType - `LoadBalancer` (default)
* Secret - `""` None (default)
* RunPort - using `EXPOSE` from the Dockerfile
* ExposePort - `80` (default)
* Replicas - `3` (default)
* RevisionHistory - `1` (default)
