# 3 Seconds to Kubernetes Manifests with Jib

How do you write your Kubernetes manifests? Do you, like most, copy/paste from the Kubernetes docs? Do you reuse an old project and update select fields? How do you instantiate a new project for deployment?

`jib` aims to solve this problem by providing its users with two options:

1. `cli` - Answer a series of questions to generate manifests
2. `infer` - Details from attributes of the project are used to derive information to generate manifests

## Cli

`jib cli --help`

The user is prompt to answer questions regarding their application:

1. What is the name of your Application?
2. What is the registry URL to access your container image?
3. Is there a cluster secret associated with your container registry?
4. What port is your application running on?
5. Would you like to expose your Application externally?
6. How many replicas would you like in the cluster? Default: 1.
7. How many historical versions of the application would you like to keep for rollback? Default: 1.

Flags are also available for an unattended option:

* `--name` - The application name
* `--registry` - The container registry URI
* `--secret` - The imagePullSecret (if private registry is used)
* `--runport` - The port the application runs on
* `--exposeport` - The port you want exposed from the luster
* `--servicetype` - The kubernetes service type
* `--replicas` - The number of replicas
* `--revisionhistory` - The number of container versions to keep

Example:

```go
bin/jib cli
  --name a
  --registry a
  --servicetype a
  --secret a
  --runport 80
  --exposeport 80
  --replicas 3
  --revisionhistory 1
```

## Infer

`jib infer --help`

Infer makes inference about the parameters required to build Kubernetes manifests. It does this by using some preset defaults, and using the project structure to define unique values. Infer also uses flags to allow the user to pass-in values dynamically at runtime.

* Name - App directory name
* Registry - Attempt to use .git/config `[remote "origin"]` url value to assume
* ServiceType - `LoadBalancer` (default)
* Secret - `""` None (default)
* RunPort - using `EXPOSE` from the Dockerfile
* ExposePort - `80` (default)
* Replicas - `3` (default)
* RevisionHistory - `1` (default)

Example

`cd project-directory && jib infer`

## Getting Started

`go get gitlab.com/sailr/opensource/jib`

`$GOPATH/bin/jib --help`

*Feature request, issues and merge requests welcome!*
